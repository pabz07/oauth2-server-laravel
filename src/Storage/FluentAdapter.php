<?php
/**
 * Fluent adapter for an OAuth 2.0 Server
 *
 * @package   lucadegasperi/oauth2-server-laravel
 * @author    Luca Degasperi <luca@lucadegasperi.com>
 * @copyright Copyright (c) Luca Degasperi
 * @licence   http://mit-license.org/
 * @link      https://github.com/lucadegasperi/oauth2-server-laravel
 */

namespace LucaDegasperi\OAuth2Server\Storage;

use League\OAuth2\Server\Storage\Adapter;
use Illuminate\Database\ConnectionResolverInterface as Resolver;

abstract class FluentAdapter extends Adapter
{
    protected $accessTokenTableName = "luca_oauth_access_tokens";
    protected $accessTokenScopeTableName = "luca_oauth_access_token_scopes";
    protected $scopeTableName = "luca_oauth_scopes";
    protected $grantTableName = "luca_oauth_grants";
    protected $grantScopeTableName = "luca_oauth_grant_scopes";
    protected $clientTableName = "luca_oauth_clients";
    protected $clientEndpointTableName = "luca_oauth_client_endpoints";
    protected $clientScopeTableName = "luca_oauth_client_scopes";
    protected $clientGrantTableName = "luca_oauth_client_grants";
    protected $sessionTableName = "luca_oauth_sessions";
    protected $sessionScopeTableName = "luca_oauth_session_scopes";
    protected $authCodeTableName = "luca_oauth_auth_codes";
    protected $authCodeScopeTableName = "luca_oauth_auth_code_scopes";
    protected $refreshTokenTableName = "luca_oauth_refresh_tokens";

    /**
     * @var \Illuminate\Database\ConnectionResolverInterface
     */
    protected $resolver;

    /**
     * @var string
     */
    protected $connectionName;

    public function __construct(Resolver $resolver)
    {
        $this->resolver = $resolver;
        $this->connectionName = null;
    }

    public function setResolver(Resolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function getResolver()
    {
        return $this->resolver;
    }

    public function setConnectionName($connectionName)
    {
        $this->connectionName = $connectionName;
    }

    protected function getConnection()
    {
        return $this->resolver->connection($this->connectionName);
    }
} 
