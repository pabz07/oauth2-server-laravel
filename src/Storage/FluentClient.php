<?php
/**
 * Fluent storage implementation for an OAuth 2.0 Client
 *
 * @package   lucadegasperi/oauth2-server-laravel
 * @author    Luca Degasperi <luca@lucadegasperi.com>
 * @copyright Copyright (c) Luca Degasperi
 * @licence   http://mit-license.org/
 * @link      https://github.com/lucadegasperi/oauth2-server-laravel
 */

namespace LucaDegasperi\OAuth2Server\Storage;

use Illuminate\Database\ConnectionResolverInterface as Resolver;
use League\OAuth2\Server\Entity\SessionEntity;
use League\OAuth2\Server\Storage\ClientInterface;
use League\OAuth2\Server\Entity\ClientEntity;
use Carbon\Carbon;

class FluentClient extends FluentAdapter implements ClientInterface
{
    /**
     * @var bool
     */
    protected $limitClientsToGrants = false;

    /**
     * @param Resolver $connection
     * @param bool $limitClientsToGrants
     */
    public function __construct(Resolver $resolver, $limitClientsToGrants = false)
    {
        parent::__construct($resolver);
        $this->limitClientsToGrants = $limitClientsToGrants;
    }

    /**
     * @return bool
     */
    public function areClientsLimitedToGrants()
    {
        return $this->limitClientsToGrants;
    }

    /**
     * @param bool $limit whether or not to limit clients to grants
     */
    public function limitClientsToGrants($limit = false)
    {
        $this->limitClientsToGrants = $limit;
    }

    /**
     * @param string $clientId
     * @param string $clientSecret
     * @param string $redirectUri
     * @param string $grantType
     * @return null|\League\OAuth2\Server\Entity\ClientEntity
     */
    public function get($clientId, $clientSecret = null, $redirectUri = null, $grantType = null)
    {
        $query = null;
        
        if (! is_null($redirectUri) && is_null($clientSecret)) {
            $query = $this->getConnection()->table($this->clientTableName)
                   ->select(
                       "{$this->clientTableName}.id as id",
                       "{$this->clientTableName}.secret as secret",
                       "{$this->clientEndpointTableName}.redirect_uri as redirect_uri",
                       "{$this->clientTableName}.name as name")
                   ->join($this->clientEndpointTableName, "{$this->clientTableName}.id", "=", "{$this->clientEndpointTableName}.client_id")
                   ->where("{$this->clientTableName}.id", $clientId)
                   ->where("{$this->clientEndpointTableName}.redirect_uri", $redirectUri);
        } elseif (! is_null($clientSecret) && is_null($redirectUri)) {
            $query = $this->getConnection()->table($this->clientTableName)
                   ->select(
                       "{$this->clientTableName}.id as id",
                       "{$this->clientTableName}.secret as secret",
                       "{$this->clientTableName}.name as name")
                   ->where("{$this->clientTableName}.id", $clientId)
                   ->where("{$this->clientTableName}.secret", $clientSecret);
        } elseif (! is_null($clientSecret) && ! is_null($redirectUri)) {
            $query = $this->getConnection()->table($this->clientTableName)
                   ->select(
                       "{$this->clientTableName}.id as id",
                       "{$this->clientTableName}.secret as secret",
                       "{$this->clientEndpointTableName}.redirect_uri as redirect_uri",
                       "{$this->clientTableName}.name as name")
                   ->join("{$this->clientEndpointTableName}", "{$this->clientTableName}.id", "=", "{$this->clientEndpointTableName}.client_id")
                   ->where("{$this->clientTableName}.id", $clientId)
                   ->where("{$this->clientTableName}.secret", $clientSecret)
                   ->where("{$this->clientEndpointTableName}.redirect_uri", $redirectUri);
        }

        if ($this->limitClientsToGrants === true and ! is_null($grantType)) {
            $query = $query->join($this->clientGrantTableName, "{$this->clientTableName}.id", "=", "{$this->clientGrantTableName}.client_id")
                   ->join($this->grantTableName, "{$this->grantTableName}.id", "=", "{$this->clientGrantTableName}.grant_id")
                   ->where("{$this->grantTableName}.id", $grantType);
        }

        $result = $query->first();

        if (is_null($result)) {
            return null;
        }

        return $this->hydrateEntity($result);
    }

    /**
     * Get the client associated with a session
     * @param  \League\OAuth2\Server\Entity\SessionEntity $session The session
     * @return null|\League\OAuth2\Server\Entity\ClientEntity
     */
    public function getBySession(SessionEntity $session)
    {
        $result = $this->getConnection()->table($this->clientTableName)
                ->select(
                    "{$this->clientTableName}.id as id",
                    "{$this->clientTableName}.secret as secret",
                    "{$this->clientTableName}.name as name")
                ->join($this->sessionTableName, "{$this->sessionTableName}.client_id", "=", "{$this->clientTableName}.id")
                ->where("{$this->sessionTableName}.id", "=", $session->getId())
                ->first();

        if (is_null($result)) {
            return null;
        }

        return $this->hydrateEntity($result);
    }

    /**
     * @param string $name The client"s unique name
     * @param string $id The client"s unique id
     * @param string $secret The clients" unique secret
     * @return int
     */
    public function create($name, $id, $secret)
    {
        return $this->getConnection()->table($this->clientTableName)->insertGetId([
            "id"  => $id,
            "name" => $name,
            "secret"   => $secret,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
    }

    /**
     * @param $result
     * @return \League\OAuth2\Server\Entity\ClientEntity
     */
    protected function hydrateEntity($result)
    {
        $client = new ClientEntity($this->getServer());
        $client->hydrate([
            "id" => $result->id,
            "name" => $result->name,
            "secret" => $result->secret,
            "redirectUri" => (isset($result->redirect_uri) ? $result->redirect_uri : null)
        ]);
        return $client;
    }
}
